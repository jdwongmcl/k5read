%% Runtest(): demontrates how to compute metabolic cost from the functions
% The two major functions are to 
% 1. read the data via readK5FromLaptop()/readK5FromPortableUnit, and 
% 2. use Brockway 1987 to compute metabolic cost, via metabolicsBreathByBreath().
%
% Here are conditions, and start and end time for each, collected in a table.
% arranged as
%[angleInDeg, metStartMin,metStopMin]
condition_data = [
  0,  22, 24;
  6,  27, 29;
  3,  32, 34;
  9,  37, 39;
  -9, 42, 44;
  -6, 52, 54;
  -3, 57, 59;
  0,  118,126.33];

condition_angles_deg  = condition_data(:,1);
times_strt_min        = condition_data(:,2);
times_end_min         = condition_data(:,3);
%% Readk5... variants: data via laptop or portable unit
% Annoyingly the files are different from the laptop and portable unit. 
% The function 'readk5AndInferSource' will handle both portable unit and laptop filetypes.
% But slightly faster is to choose either readK5FromLaptop() or readK5FromPortableUnit() 
% directly.
[k5data,bbb]  = readK5AndInferSource(fname); 
mass          = 55;
metdata_avgs  = metabolicsBreathByBreath(bbb,times_end_min,'mass',mass,'verbose',1);
%% Plot Watts as a function of Condition
% MetabolicsBreathByBreath() returns the averaged data, which we plot against degrees (in this case).
figure();
plot(condition_angles_deg,metdata_avgs.wattsO2CO2,'b.','markersize',10);
set(gcf(),'color','w');
box off;
xlabel('Incline (degrees)');
ylabel('Gross Metabolic Power (W)');
out = [condition_angles_deg,metdata_avgs.wattsO2CO2];