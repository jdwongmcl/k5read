% add to the path the subfolders 'src' and 'test', and run runtest.

% first update the path.
[folder,~] = fileparts(mfilename());
addpath(fullfile(folder,'src'));
addpath(fullfile(folder,'test'));

% run the test data and save a pdf of the result.
publish('runtest.m','format','pdf','outputDir','reports');