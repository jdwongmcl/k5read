
#%%
import numpy as np
import pandas as pd
import os,sys # os does the path joining, sys adds to the path
import matplotlib.pyplot as plt
import matplotlib
import k5read as k5

# for later: define subject class that has all sub-specific data.
class SubDat():
  file = ''
  time_ends_min = []
  conditions = []

# def SubDat(self,fname,tendsmin,cond):

#%%
data_dir = '/Users/jeremy/Library/CloudStorage/OneDrive-UniversityofCalgary/Project Older Adults Efficiency/data/respirometery'
fname = ['20240307 diane wilson (CPET Breath by Breath)_20240307124551.xlsx']
time_ends_min = [3,8,12.33,16.5,21,25.67]
cond_bpm = np.array([0,58,35,70,47,82])

# build filepath
file_path = os.path.join(data_dir,fname[0])
cond_fs_dia  = cond_bpm*2/60
b1_dia,b2_dia,dframe_dia = k5.brockway(file_path,time_ends_min)
cond_name = 'Freq (Hz )'
k5.plot_brockway_results(dframe_dia,time_ends_min,b1_dia,b2_dia,cond_fs_dia,cond_name,avg_time_minutes=3)



#          '20240308 Caroline unsure (CPET Breath by Breath)_20240308123244.xlsx']
#%%
fname = ['20240307 stew wilson (CPET Breath by Breath)_20240307120432.xlsx']
time_ends_min = [5.33,26.5,34,42,47.5,53.5]
cond_bpm = np.array([0,35,58,82,70,47])

file_path = os.path.join(data_dir,fname[0])
b1_ste,b2_ste,dframe = k5.brockway(file_path,time_ends_min)
cond_fs_ste  = cond_bpm*2/60
cond_name = 'Freq (Hz )'
k5.plot_brockway_results(dframe,time_ends_min,b1_ste,b2_ste,cond_fs_ste,cond_name,avg_time_minutes=3)

#%%
fname = ['20240308 Caroline unsure (CPET Breath by Breath)_20240308123244.xlsx']
time_ends_min = [4,21.5,26,29.5,33,36.5]
cond_bpm = np.array([0,35,82,70,58,47])

file_path = os.path.join(data_dir,fname[0])
b1_car,b2_car,dframe_car = k5.brockway(file_path,time_ends_min)
cond_fs_car  = cond_bpm*2/60
cond_name = 'Freq (Hz )'
k5.plot_brockway_results(dframe,time_ends_min,b1_car,b2_car,cond_fs_car,cond_name,avg_time_minutes=3)

# %%
fname = ['/Users/jeremy/Library/CloudStorage/OneDrive-UniversityofCalgary/Project Older Adults Efficiency/data/respirometery/20240308 Bruce Alger (CPET Breath by Breath)_20240308165725.xlsx']
time_ends_min = [13,21.17,26.75,32.33,38.67,44.33]
cond_bpm = np.array([0,47,58,70,82,35])

cond_fs_bru  = cond_bpm*2/60
file_path = os.path.join(data_dir,fname[0])
b1_bru,b2_bru,dframe_bru = k5.brockway(file_path,time_ends_min)
cond_name = 'Freq (Hz )'
k5.plot_brockway_results(dframe_bru,time_ends_min,b1_bru,b2_bru,cond_fs_bru,cond_name,avg_time_minutes=3)

# %%
conds_all = [cond_fs_dia,cond_fs_ste,cond_fs_car,cond_fs_bru]
b1_all = [b1_dia,b1_ste,b1_car,b1_bru]
#%%
b1_sort_all = np.zeros((len(conds_all),len(conds_all[0])))
for (i_conds,conds) in enumerate(conds_all):
  # sort each b1 according to cond, and store in new variable b1_sort
  b1_sort = np.zeros(len(conds))
  for (i_cond,cond) in enumerate(conds):
    idx = np.argsort(conds)
    b1_sort = b1_all[i_conds][idx]
  b1_sort_all[i_conds,:] = b1_sort
# %%
met_mean = np.mean(b1_sort_all,axis = 0)
fs = np.sort(conds_all[0])
plt.plot(fs,met_mean)
# %%
