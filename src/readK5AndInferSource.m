function [output,bbb]=readK5AndInferSource(fname)
% function [output,bbb]=readK5AndInferSource(fname)
% read data and calls first readK5FromPortable(), then readK5FromLaptop() to
% handle either type of file.
% Maybe this info is stored in the metadata. For now we just check O2
% column.
% 
% Inputs: filename of file to be read.
% outputs: two structs:
% 1:all-columns output 
% 2:bbb. the first is all of the data in the data table, 
% while the second (bbb) is only 
% 1. time 
% 2. o2 [ml/min]
% 3. co2 [ml/min]
% 4. RQ
% which are all that are needed for metabolicsBreathByBreath().
% (RQ is used in some formulas for breathbybreath; Brockway 1987 (the simplest) ignores RQ.
 
fprintf('Reading as portable data first.\n');
[output,bbb] = readK5FromPortableUnit(fname);

if isequal('cell',class(bbb.o2mlPmin))
  fprintf('Oops. DeltaO2 column is text...so data likely collected on the laptop.\n');
  fprintf('Returning via readK5FromLaptop().\n');
  [output,bbb] = readK5FromLaptop(fname);
end
  
