#%%
import numpy as np
import pandas as pd
import os,sys # os does the path joining, sys adds to the path
import matplotlib.pyplot as plt
import matplotlib
# %matplotlib inline

def brockway(file_path,end_time_minutes,avg_time_minutes=3):
  """
  This function takes in a file path and a list of end times (in minutes). 
  it estimates metabolic cost by summing the flow-rates of expired breaths over
  the preceeding x minutes. 
  def brockway(file_path,end_time_minutes):
  file_path: the path to the file
  end_time_minutes: the time in minutes at which to end the averaging
  returns: brockway1, brockway2, df 
  """

  # import the xls file, but only read some columns:
  cols  = [9,10,11,12,13,14,15,16]
  
  # columns are breath time, respiratory frequency, tidal volume, expiratory volume per min, inspiratory volume mL, vo2 mlpermin, vco2 mlpermin, rq VCO2/VO2
  colnames = ['t','rf_permin',	'vt_l',	've_lpermin',	'iv',	'vo2_mlpermin',	'vco2_mlpermin',	'rq']
  df = pd.read_excel(file_path,skiprows=2,usecols=cols, names = colnames)
  # df is now a big table or spreadsheet.
  # each row has a time at which it took place -- an expired breath -- and
  # breath-by-breath values. 

  # convert vo2 and vco2 ml/min to L/s
  vo2   = df["vo2_mlpermin"]/60000
  vco2  = df["vco2_mlpermin"]/60000
  
  # convert t which is a series object to units of seconds
  t_s = df["t"]
  def time_to_seconds(time_obj):
      return time_obj.hour * 3600 + time_obj.minute * 60 + time_obj.second
  t_s = t_s.apply(time_to_seconds)

  # add t_s, vo2, and vco2 to the dataframe (to return them later if needed)
  df["t_s"]         = t_s
  df["vo2"]         = vo2
  df["vco2"]        = vco2
  df["brockway1"]  = 21.00*np.array(df["vo2"])*1000 # flow of o2 in L, 21->returns kJ, we want J so x1000.
  df["brockway2"]  = 16.58*np.array(df["vo2"])*1000 + 4.51*np.array(df["vco2"])*1000 # as above

  # if we wanted, we could plot this data as ml/min/kg to double-check
  # these numbers would be around 2.5-5 for an avg subject.
  #plt.plot(t,vo2*1000*60/mass)
  
  # convert the input end_time_minutes to seconds 
  end_time_s = [x*60 for x in end_time_minutes]
  avg_time_s = avg_time_minutes*60

  # in the following for-loop:
  # for each avg_time_s (i.e. for each of our 5 beep conditions, where we recorded the end-time):
  # 1. find the indices that we want to average over, stored as idx.
  # 2. take the average of the vo2 values at those indices, 
  # scaled by the time difference between each point (the breath dur)
  # Note 1: ignore the first breath, because python's diff gives nan for t_tmp.diff()[0].
  # Note 2: if we just averaged across time, we would be weighting each breath equally,
  # but we want to weight each breath by the time it takes, so we divide by the total time.
  # We will see that ultimately it doesn't affect the results much.
  vo2_avg = []
  vco2_avg = []
  for the_time_s in end_time_s:
    start_time = the_time_s - avg_time_s
    idx = (t_s > start_time) & (t_s < the_time_s)
    # since the times are not equal,
    # weight each value by the time difference
    # then sum
    t_tmp = t_s[idx]
    vo2_tmp = vo2[idx]
    vco2_tmp = vco2[idx]
    
    # the time for each breath is the difference between the times
    t_brth = t_tmp.diff()
    t_brth = t_brth.fillna(0)

    # total time 
    # note: we need to ignore the first breath, because python's diff gives nan for t_tmp.diff()[0].
    t_total = np.sum(t_brth[1:])

    # add to the list
    vo2_avg.append(np.sum(vo2_tmp*t_brth)/t_total)
    vco2_avg.append(np.sum(vco2_tmp*t_brth)/t_total) 

  # now we have the average vo2 and vco2 for each time period
  brockway1 = 21.00*np.array(vo2_avg)*1000                         # brockway 1987. flow of o2 in L, 21->returns kJ, we want J so x1000.
  brockway2 = 16.58*np.array(vo2_avg)*1000.0 + 4.51*np.array(vco2_avg)*1000.0 # volume in co2 in L, returns in kJ-> returns kJ, we want J so x1000.

  return (brockway1, brockway2,df)

def plot_brockway_results(df,end_time_minutes,b1,b2,cond_vec,cond_str,avg_time_minutes=3):
  """
  This function takes in the results of the brockway function and plots them.
  """
  fig,ax = plt.subplots(2,2)

  ax[0,0].plot(cond_vec,b1,marker = 'o',linestyle = 'none')
  ax[0,0].plot(cond_vec,b2,marker = 'x',linestyle='none')
  ax[0,0].set_xlabel(cond_str)
  ax[0,0].set_ylabel('Metabolic rate (W)')
  ax[0,0].legend(['Brockway 1','Brockway 2'])
  
  # use df['t_s'] to plot the time series of vo2 and vco2 in 
  # subplots [0,1] and [1,1] respectively
  ax[0,1].plot(df['t_s'],df['vo2'])
  ax[1,1].plot(df['t_s'],df['vco2'])
  ax[0,1].set_xlabel('Time (s)')
  ax[1,1].set_xlabel('Time (s)')
  ax[0,1].set_ylabel('VO2 (L/s)')
  ax[1,1].set_ylabel('VCO2 (L/s)')

  # for each of the end_time_minutes, plot the average vo2 and vco2
  # in subplots [1,0] and [1,1] respectively
  ax[1,0].plot(df['t_s'],df['brockway1'])
  ax[1,0].plot(df['t_s'],df['brockway2'])
  for i in range(len(end_time_minutes)):
    # compute the end_time_s range going back avg_time_minutes
    end_time_s = end_time_minutes[i]*60
    start_time = end_time_s - avg_time_minutes*60
    idx = (df['t_s'] > start_time) & (df['t_s'] < end_time_s)
    # compute average over that range
    # note: these are slightly wrong, 
    # because here we are not weighting each breath by the time it takes
    bwavg = np.mean(df['brockway1'][idx])
    ax[1,0].plot(df["t_s"][idx],np.repeat(bwavg,len(df['brockway1'][idx])),'k',linewidth=10)
    # now we repmat the values from the analysis and plot them
    b1rep = np.repeat(b1[i],len(df['brockway1'][idx]))
    b2rep = np.repeat(b1[i],len(df['brockway2'][idx]))
    ax[1,0].plot(df["t_s"][idx],b1rep,color ='blue',linestyle = '-.',linewidth=2,marker='o')
    ax[1,0].plot(df["t_s"][idx],b2rep,color='red', linestyle = ':',linewidth=1,marker='x')
  ax[1,0].legend(['brk1','brk2','byb','b1','b2'])

  ax[1,0].set_ylabel('Metabolic rate (W)')


  ax[1,0].set_xlabel('Time (s)')
  #set tight layout
  plt.tight_layout()
  plt.show()

##### below we show how to use the function brockway() #####
time_ends_min = [24.86,31.0,36.75,42,48]
file_demo = '20240202 Jeremy Wong (CPET Breath by Breath)_20240202151036.xlsx'
avg_time_minutes = 3

# the directory containing the current file. this will change if you 
# are not using the git repo as it was designed originally.
# as is, the file_demo must be inside a directory called 'data' inside the directory containing this file.
file_path = os.path.join(os.path.dirname(__file__),'data',file_demo)

# call the function
b1,b2,dframe = brockway(file_path,time_ends_min)

# now call the plotting function
# the plotting function takes in additional arguments: condition and condition name. 
# thes are used to plot on the first plot the metabolic rate as a function of the condition.
# in these example data, the metabolic rate is a function of frequency.
cond_name = 'Freq (Hz )'
fs = np.array([2.456021,   1.79536224, 1.37011782, 1.0862075,  0.88796024])

# plot_brockway_results(dframe,time_ends_min,b1,b2,fs,cond_name,avg_time_minutes=3)
# %%
